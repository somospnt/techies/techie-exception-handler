package com.somospnt.techieexceptionhandler.controller.rest;

import com.somospnt.techieexceptionhandler.exception.TechieException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ExceptionHandlerRestController {

    @GetMapping("api/exception-handler")
    public String explotar() {
        throw new TechieException("Explotó el bichito con exception handler!");
    }

    @ExceptionHandler({TechieException.class})
    public ResponseEntity handleException(TechieException exception) {
        log.error("Ocurrió un error en la APP", exception);
        return new ResponseEntity(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
