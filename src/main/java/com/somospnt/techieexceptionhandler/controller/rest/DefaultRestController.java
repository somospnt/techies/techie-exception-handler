package com.somospnt.techieexceptionhandler.controller.rest;

import com.somospnt.techieexceptionhandler.exception.TechieException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultRestController {

    @GetMapping("api/default")
    public String explotar() {
        throw new TechieException("Explotó el bichito por default!");
    }

}
