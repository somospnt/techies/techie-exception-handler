package com.somospnt.techieexceptionhandler.controller.rest;

import com.somospnt.techieexceptionhandler.exception.TechieException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController
public class ResponseStatusExceptionRestController {

    @GetMapping("api/response-status-exception-bad-request")
    public String explotarBadRequest() {
        try {
            throw new TechieException("Explotó el bichito con response status exception!");
        } catch (TechieException exception) {
            log.error("Ocurrió un error en la APP", exception);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage(), exception);
        }
    }

    @GetMapping("api/response-status-exception-teapot")
    public String explotarSoyUnaTetera() {
        try {
            throw new TechieException("Explotó el bichito con response status exception!");
        } catch (TechieException exception) {
            log.error("Ocurrió un error en la APP", exception);
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, exception.getMessage(), exception);
        }
    }
    
}
