//package com.somospnt.techieexceptionhandler.controller.advice;
//
//import com.somospnt.techieexceptionhandler.exception.TechieException;
//import java.io.IOException;
//import javax.servlet.http.HttpServletResponse;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//
//@Slf4j
//@ControllerAdvice
//public class ExceptionControllerAdvice {
//
//    @ExceptionHandler({TechieException.class})
//    public ResponseEntity handleException(TechieException exception) throws IOException {
//        log.error("Ocurrió un error en la APP", exception);
//        return new ResponseEntity(exception.getMessage(), HttpStatus.BAD_REQUEST);
//    }
//
////    @ExceptionHandler({TechieException.class})
////    public void handleException(TechieException exception, HttpServletResponse response) throws IOException {
////        log.error("Ocurrió un error en la APP", exception);
////        response.sendError(HttpStatus.I_AM_A_TEAPOT.value(), exception.getMessage());
////    }
//
//}
