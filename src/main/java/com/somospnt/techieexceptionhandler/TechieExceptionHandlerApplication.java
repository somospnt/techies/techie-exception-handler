package com.somospnt.techieexceptionhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieExceptionHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieExceptionHandlerApplication.class, args);
	}

}
