package com.somospnt.techieexceptionhandler.exception;

public class TechieException extends RuntimeException {

    public TechieException() {
        super();
    }

    public TechieException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechieException(String message) {
        super(message);
    }

    public TechieException(Throwable cause) {
        super(cause);
    }
}
